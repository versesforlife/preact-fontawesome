# preact-fontawesome

[![npm version](https://badge.fury.io/js/%40versesforlife%2Fpreact-fontawesome.svg)](https://www.npmjs.com/package/@versesforlife/preact-fontawesome)

> Preact module for Font Awesome 5 icons

This is an unofficial Preact module for Font Awesome 5.  It's unofficial in the sense that it's not created or supported by FortAwesome and is a community contribution.

Please see the official Font Awesome 5 React module for [README](https://github.com/FortAwesome/react-fontawesome/blob/master/README.md) details and information about how to use. This package repackages `react-fontawesome` for Preact compatibility.

## Differences from React version

Instead of importing from `react`, import from `preact`. See examples in original, and modify as appropriate such as the following:

```javascript
import { h } from 'preact';
import { FontAwesomeIcon } from 'preact-fontawesome';
```

## How this is built

This module uses a rollup build process which aliases `react` and `react-dom` imports to use `preact` instead.

## Browser support

Using `babel-preset-env` for browser support, this targets: `'> 1%', 'last 2 versions', 'ie > 10'`.
