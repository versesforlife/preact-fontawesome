import alias from '@rollup/plugin-alias'
import commonJs from '@rollup/plugin-commonjs'
import resolve from '@rollup/plugin-node-resolve'
import path from 'path'
import babel from 'rollup-plugin-babel'

const DIST = path.resolve(__dirname, 'dist')

const name = 'preact-fontawesome'
const globals = {
  '@fortawesome/fontawesome-svg-core': 'FontAwesome',
  'preact': 'React',
  'prop-types': 'PropTypes'
}

export default {
  external: [
    '@fortawesome/fontawesome-svg-core',
    'prop-types',
    'preact'
  ],
  input: './node_modules/@fortawesome/react-fontawesome/index.js',
  output: [
    {
      name,
      globals,
      format: 'umd',
      file: path.resolve(DIST, 'index.js')
    },
    {
      name,
      globals,
      format: 'es',
      file: path.resolve(DIST, 'index.es.js')
    }
  ],
  plugins: [
    alias({
      entries: [
        {find: 'react', replacement: 'preact'},
        {find: 'react-dom', replacement: 'preact'},
      ]
    }),
    resolve({
      jsnext: true,
      main: true
    }),
    commonJs(),
    babel({
      babelrc: false,
      presets: [
        [
          'env',
          {
            debug: true,
            targets: { browsers: ['> 1%', 'last 2 versions', 'ie > 10'] },
            modules: false
          }
        ],
        'preact'
      ],
      plugins: ['external-helpers'],
      exclude: 'node_modules/**'
    })
  ]
}
